//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

import * as PIXI from 'pixi.js';
global.PIXI = PIXI;
require('pixi-layers')


import { App } from "@app"
global.App = new App();
