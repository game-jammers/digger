//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

import { TilesetDef } from './tilesets';

import * as PIXI from 'pixi.js';

//
// ////////////////////////////////////////////////////////////////////////////
//

const ASSET_TYPE = {
  Tileset: 'tilesets',
}

const ASSET_IDS = {
  FlatGrass:      { type: ASSET_TYPE.Tileset, tileset: 'proto', id: '90.png' },
  FlatDirt:       { type: ASSET_TYPE.Tileset, tileset: 'proto', id: '98.png' },
  FlatSand:       { type: ASSET_TYPE.Tileset, tileset: 'proto', id: '74.png' },
  FlatConcrete:   { type: ASSET_TYPE.Tileset, tileset: 'proto', id: '96.png' }
}

//
// ////////////////////////////////////////////////////////////////////////////
//

export class AssetManager {

  //
  // constructor //////////////////////////////////////////////////////////////
  //

  constructor(readyCallback) {
    this.ready = false;
    this.readyCallback = readyCallback;
    
    const loader = PIXI.Loader.shared;

    this.ids = ASSET_IDS;
    this.tilesets = {};

    //
    // load tilesets
    //
    for(var idx in TilesetDef) {
      const def = TilesetDef[idx];
      loader.add(def.name, def.uri);
    }

    this._onLoadComplete = this._onLoadComplete.bind(this);

    //
    // do this in a timeout so the constructor can complete before
    // the load completes
    //
    setTimeout(()=>{
      loader.load(this._onLoadComplete)
    }, 10);
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  getAsset(id) {
    switch(id.type) {

      //
      // tileset getter
      //
      case ASSET_TYPE.Tileset: {
        return this.tilesets[id.tileset].textures[id.id];
      }
      break;

    }
  }

  //
  // ------------------------------------------------------------------------
  //
  
  getSprite(id) {
    return new PIXI.Sprite(this.getAsset(id));
  }
  

  //
  // private methods //////////////////////////////////////////////////////////
  //
  
  _onLoadComplete(loader, resources) {

    //
    // mount tilesets
    //
    for(var idx in TilesetDef) {
      const def = TilesetDef[idx];
      this.tilesets[def.name] = resources[def.name];
    }

    this.ready = true;
    if(this.readyCallback) {
      this.readyCallback()
    }
  }
}
