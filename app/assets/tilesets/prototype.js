//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

//
// ------------------------------------------------------------------------
//

export const PrototypeTilesetDef = {
  name: 'proto',
  uri: 'assets/tilesets/prototype/prototype.json'
}
