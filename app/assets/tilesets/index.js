//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

import { PrototypeTilesetDef } from './prototype';

export const TilesetDef = [
  PrototypeTilesetDef
]
