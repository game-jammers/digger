//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

import "./style/index.styl"
import { AssetManager } from './assets';
import { RenderSystem, TileMap } from './render';
import * as PIXI from 'pixi.js';

//
// constants //////////////////////////////////////////////////////////////////
//

const TILESIZE = { x: 64, y: 32 };

//
// App ////////////////////////////////////////////////////////////////////////
//

export class App {

  //
  // constructor //////////////////////////////////////////////////////////////
  //

  constructor() {
    this.start = this.start.bind(this);
    this.renderer = new RenderSystem();
    this.assets = new AssetManager(this.start);
    this.total = 0;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  start() {
    this.renderer.start();
    this.tilemap = new TileMap(this, this.renderer.stage, TILESIZE);

    const size = { 
      x: Math.ceil(this.renderer.size.x / TILESIZE.x), 
      y: Math.ceil(this.renderer.size.y / TILESIZE.y*2) 
    };

    this.renderer.stage.position.x = this.renderer.size.x/2;
    this.renderer.stage.position.y = this.renderer.size.y/2;

    for(var y = -(size.x); y < size.x; ++y) {
      for( var x = -(size.y); x < size.y; ++x ) {
        this.tilemap.set(this.assets.ids.FlatGrass, {x, y, z: 0});
      }
    }

    for(var t = 0; t < 35; t++)
    {
      var curpos = { x: 0, y: 0, z: t % 2 + 1 };
      for(var i = 0; i < 150; i++) {
        this.tilemap.set(this.assets.ids.FlatConcrete, curpos);
        if(curpos.z > 1) {
          this.tilemap.set(this.assets.ids.FlatDirt, {x: curpos.x, y: curpos.y, z: 1});
        }

        if(Math.random() > 0.5) {
          curpos.x += Math.round(Math.random())*2-1;
        }
        else {
          curpos.y += Math.round(Math.random())*2-1;
        }
      }
    }

    this.ticker = new PIXI.Ticker();
    this.ticker.add((dt)=>{
      this.update(dt)
    });

    this.ticker.start();
  }

  //
  // --------------------------------------------------------------------------
  //

  update(dt) {
    this.renderer.update(dt);
    this.total += dt;

    const halfsize = { x: this.renderer.size.x/2, y: this.renderer.size.y/2 };
    const pos = { 
      x: Math.sin(this.total/200) * halfsize.x + halfsize.x,
      y: Math.cos(this.total/200) * halfsize.y + halfsize.y
    };

    this.renderer.stage.position.x = pos.x;
    this.renderer.stage.position.y = pos.y;
  }
  
  //
  // --------------------------------------------------------------------------
  //

  shutdown() {
  }
};
