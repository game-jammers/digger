//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

import * as PIXI from 'pixi.js';

//
// A 3d grid of tiles represented as a set of isometric 2d sprites.
//

export class TileMap {

  //
  // constructor //////////////////////////////////////////////////////////////
  //

  constructor(app, stage, tileSize) {
    this.app = app;
    this.stage = stage;
    this._tiles = []
    this.tileSize = tileSize;
    this.halfTileSize = { x: tileSize.x/2-1, y: tileSize.y/3-1 };
    this.dims = {x: 0, y: 0, z: 0};
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  map2screen(pos) {
    return {
      x: (pos.x - pos.y) * this.halfTileSize.x,
      y: (pos.x + pos.y) * this.halfTileSize.y - (pos.z * this.halfTileSize.y/2)
    };
  }

  //
  // --------------------------------------------------------------------------
  //
  
  screen2map(pos) {
    console.log(pos);
    return {
      x: (pos.x / this.halfTileSize.x + pos.y / this.halfTileSize.y) / 2,
      y: (pos.y / this.halfTileSize.y - pos.x / this.halfTileSize.x) / 2
    };
  }

  //
  // --------------------------------------------------------------------------
  //
  
  set(id, pos) {
    const col = this._tiles[pos.x] || [];
    this._tiles[pos.y] = col;

    const row = col[pos.y] || [];
    col[pos.y] = row;

    const sprite = this.app.assets.getSprite(id);
    sprite.width = this.tileSize.x;
    sprite.height = this.tileSize.y;
    const screenPos = this.map2screen(pos);
    sprite.position.x = screenPos.x;
    sprite.position.y = screenPos.y;
    sprite.zIndex = sprite.position.y + (10000 * pos.z);

    this.stage.addChild(sprite);

    row[pos.z] = sprite;
  }

  //
  // --------------------------------------------------------------------------
  //

  get(pos) {
    const col = this._tiles[pos.x] || [];
    const row = col[pos.y] || [];
    return row[pos.z] || null;
  }
}

