//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

import * as PIXI from 'pixi.js';

export class RenderSystem {

  //
  // constructor //////////////////////////////////////////////////////////////
  //
  
  constructor() {
    window.addEventListener('resize', ()=>{
      this.resize();
    })

    this.mount = document.getElementById('mount');

    this.app = new PIXI.Application(window.innerWidth, window.innerHeight, {
      backgroundColor: 0xff00ff
    });

    this.stage = new PIXI.Container();
    this.stage.sortableChildren = true;
    this.app.stage.addChild(this.stage);
  }

  //
  // accessors ////////////////////////////////////////////////////////////////
  //

  get size() {
    return { x: this.mount.clientWidth, y: this.mount.clientHeight };
  } 

  //
  // public methods ///////////////////////////////////////////////////////////
  //
  
  start() {
    this.mount.appendChild(this.app.view);
    this.resize();
  }

  //
  // --------------------------------------------------------------------------
  //
  
  update(dt) {
    this.app.render();
  }

  //
  // --------------------------------------------------------------------------
  //
  
  shutdown() {
    this.app.destroy();
  }

  //
  // --------------------------------------------------------------------------
  //
  
  resize() {
    const size = this.size;
    this.app.renderer.resize(size.x, size.y);
  }

  //
  // private methods //////////////////////////////////////////////////////////
  //
}
