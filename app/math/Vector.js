//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

import * as glMatrix from 'gl-matrix';

export const vec2 = glMatrix.vec2;
export const vec3 = glMatrix.vec3;
