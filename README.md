# digger

Digger game jam

## Dependencies

- NodeJS v12.13.0
- (optional) Yarn

## Installation

- `yarn` or `npm install`
- `yarn dev`
