//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

//
// constants //////////////////////////////////////////////////////////////////
//

const ROOT_DIR = path.resolve(__dirname);
const DIST_DIR = path.join(ROOT_DIR,'dist');
const PUBLIC_DIR = path.join(ROOT_DIR,'public');
const isProd = process.env.NODE_ENV === 'production';
const isDev = !isProd;

//
// ////////////////////////////////////////////////////////////////////////////
//

module.exports = {
  entry: './index.js',
  output: {
    filename: 'main.js', 
    path: DIST_DIR
  },
  mode: isDev ? 'development' : 'production',

  //
  // ------------------------------------------------------------------------
  //
  
  resolve: {
    alias: {
      '@app': path.join(ROOT_DIR, 'app'),
    }
  },

  //
  // ------------------------------------------------------------------------
  //
  
  module: {
    rules: [

      //
      // babel loader
      //
      { test: /\.js$/, exclude: path.join(ROOT_DIR,'node_modules'), loader: 'babel-loader' },

      //
      // styles
      //
      {
        test: /\.styl$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'stylus-loader' }
        ]
      },
    ]
  },

  //
  // --------------------------------------------------------------------------
  //

  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(ROOT_DIR, "index.html")
    }),

    new CopyWebpackPlugin([
      {from: 'assets', to: path.join(DIST_DIR, 'assets')},
    ])
  ],
  
  //
  // --------------------------------------------------------------------------
  //
  
  devServer: {
    contentBase: [DIST_DIR, PUBLIC_DIR],
    index: 'index.html',
    compress: true,
    port: 9000,
  },
}
