//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

//
// constants //////////////////////////////////////////////////////////////////
//

const devEnvironments = [
  'development'
];

//
// config /////////////////////////////////////////////////////////////////////
//

module.exports = function(api) {
  const isDev = api.env(devEnvironments)
  return {
    plugins: [
      "@babel/plugin-proposal-export-default-from"
    ],
    presets: [
      '@babel/preset-env'
    ]
  };
}
